//Reading property file
var propertyreader = require('properties-reader')
var configDirectory = './configs';
var properties = propertyreader(configDirectory + '/db.properties');

var url = properties.get('mongo.dev.local')//connection url
var mongoDb = require('mongodb');
var mongoClient = mongoDb.MongoClient;
var dbname = properties.get('mongo.dev.dbname')

var connection = {
    db: null
}

module.exports.dbConnect = function () {
    mongoClient.connect(url,{useNewUrlParser: true, useUnifiedTopology: true}, function (err, client) {
        if (err) {
            console.log(err);
            process.exit(1)
        }
        else {
            var db = client.db(dbname);
            connection.db = db;
            console.log('db connected successfully')
        }
    })
}//dbConnect

module.exports.getDBConnection = function () {
    return connection.db;
};//getDBConnection