var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var http = require('http');
var cors = require("cors");

var propertyreader = require('properties-reader');
var configDirectory = './configs';
var properties = propertyreader(configDirectory + '/config.properties');
var port = properties.get('app.server.port');

var routes = require('./src/routes/index.js');
// var cards = require('./src/routes/cards.js');
// var usercard = require('./src/routes/usercards');

var originsWhitelist = [
    "*",
    "http://localhost:4200", // this is my front-end url for development
 
  ];

var corsOptions = {
    origin: function (origin, callback) {
      var isWhitelisted = originsWhitelist.indexOf(origin) !== -1;
      callback(null, isWhitelisted);
    },
    credentials: true,
  };
app.use(cors(corsOptions));


var db = require('./db')
var mydb;
db.dbConnect()
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.listen(port, () => console.log(`Example app listening on port ${port}!`))
app.use('/',routes);
// app.use('/card',cards);
// app.use('/usercard',usercard);


module.exports = app;